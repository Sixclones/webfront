import { html } from './html.js'

import Slider from './Slider.js';
import Hamburger from './Hamburger.js';
import Filter from './Filter.js';
import Scroll from './Scroll.js';

import { style } from './../sass/index.scss';

const news = new Slider(
	'slider',
	'slider.json'
);
const hamburger = new Hamburger('.hamburger', '.header');

const seller = new Filter('seller');
const catalog = new Filter('catalog');

const scroll = new Scroll();
window.addEventListener('load', () => {
	scroll.addNewElements('#seller .product', window.innerHeight * 0.8);
	scroll.addNewElements('#catalog .product', window.innerHeight * 0.8);
	scroll.addNewElements('.historic-part_left .historic-infobloc', window.innerHeight * 0.75);
	scroll.addNewElements('.historic-part_right .historic-infobloc', window.innerHeight * 0.75);
});