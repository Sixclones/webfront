document.body.innerHTML = require('../hbs/home.hbs')();
var templates = ['header', 'landing', 'seller', 'news', 'historic', 'stats', 'catalog', 'footer'];

for (let i = 0; i < templates.length; i++) {
	document.getElementsByClassName(templates[i])[0].innerHTML = require('../hbs/' + templates[i] + '.hbs')();
}