import TweenMax from 'gsap/TweenMax';
import getHttpRequest from './Ajax.js';

export default class Slider {
	constructor(_container, _file) {
		this.container = document.getElementById(_container);
		this.file = _file;
		this.datas;
		this.state = {
			position: 1,
			animated: false
		};
		this.anim = 0.6;
		this.left = document.querySelector('.slider-arrow_left');
		this.right = document.querySelector('.slider-arrow_right');
		this.values = [{
				wrapper: document.getElementsByClassName('slider-cover')[0],
				tag: document.createElement('img'),
				key: 'img'
			},
			{
				wrapper: document.getElementsByClassName('slider-title')[0],
				tag: document.createElement('h2'),
				key: 'title'
			},
			{
				wrapper: document.getElementsByClassName('slider-description')[0],
				tag: document.createElement('p'),
				key: 'description'
			},
			{
				wrapper: document.getElementsByClassName('slider-pagination')[0],
				tag: document.createElement('img'),
				key: 'thumbnail'
			}
		]
		
		this.load();
	}
	load() {
		this.httpRequest = getHttpRequest();
		this.httpRequest.open('GET', this.file);
		this.httpRequest.send();
		this.httpRequest.addEventListener('readystatechange', this.loaded.bind(this));
	}
	loaded() {
		if (this.httpRequest.readyState === 4) {
			if (this.httpRequest.status === 200) {
				this.datas = JSON.parse(this.httpRequest.responseText);
				this.build();
				this.max = this.datas.length;
				this.listen();
				} else {
				console.log('Erreur');
			}
		}
	}
	build() {
		this.values.forEach((value, i) => {
			value.tag.classList.add(value.wrapper.className + '-item');
			var nodes = [];
			this.datas.forEach((data, j) => {
				nodes.push(this.values[i].tag.cloneNode(true));
				if (this.values[i].tag.tagName === 'IMG') {
					nodes[j].src = data[this.values[i].key];
				} else {
					nodes[j].innerHTML = data[this.values[i].key];
				}
				nodes[j].dataset.id = data.id;
				if (this.state.position === data.id) {
					nodes[j].classList.add('active');
				}
				this.values[i].wrapper.appendChild(nodes[j]);
			});
		});
	}
	listen() {
		this.left.addEventListener('click', () => { this.move(-1) });
		this.right.addEventListener('click', () => { this.move(1) });
		this.values
			.filter(value => value.key === 'thumbnail')[0]
			.wrapper.childNodes.forEach(item => {
				if (item.nodeType === 1) {
					item.addEventListener('click', () => { this.move(item.dataset.id - this.state.position) });
				}
			});
	}
	move(direction) {
		const next = Math.max(1, Math.min(this.max, this.state.position + direction))
		if (!this.state.animated && next !== this.state.position) {
			this.state.animated = true;
			const height = this.container.clientHeight;
			let textHeight;
			this.values.forEach(value => {
				if (value.wrapper.classList.contains('slider-description')) {
					textHeight = value.wrapper.clientHeight;
				}
				value.wrapper.childNodes.forEach(item => {
					if (item.nodeType === 1 && item.dataset.id == this.state.position) {
						item.classList.remove('active');
						if (item.classList.contains('slider-pagination-item')) {
							TweenMax.to(item, this.anim, { scale: 1 });
						}
					}
				});
			});
			this.state.position = next;
			this.values.forEach(value => {
				value.wrapper.childNodes.forEach(item => {
					if (item.nodeType === 1 && item.dataset.id == this.state.position) {
						item.classList.add('active');
						if (item.classList.contains('slider-pagination-item')) {
							TweenMax.to(item, this.anim, { scale: 1.25 });
						} else {
							TweenMax.fromTo(item, this.anim, { autoAlpha: 0}, { autoAlpha: 1});
						}
					}
				});
				if (value.wrapper.classList.contains('slider-description')) {
					TweenMax.fromTo(
						value.wrapper, this.anim,
						{ height: textHeight },
						{
							height: value.wrapper.clientHeight,
							onComplete: () => {
								value.wrapper.style.height = '';
								this.state.animated = false;
							}
						});
				}
			});
		}
	}
}