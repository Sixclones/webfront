import TweenMax from 'gsap/TweenMax';

export default class Scroll {
	constructor() {
		this.update = this.update.bind(this);

		this.elements = [];

		this.update();
	}
	addNewElements(_el, _triggered) {
		this.elements.push({
			el: document.querySelectorAll(_el),
			triggered: _triggered,
			animated: false,
			pos: null
		});
		this.elements.forEach(element => {
			element.el.forEach(el => { el.style.opacity = 0 })
			element.pos = element.el[0].getBoundingClientRect().top;
		});
	}
	update() {
		this.elements.forEach(element => {
			if (!element.animated && element.triggered + window.pageYOffset >= element.pos) {
				element.animated = true;
				TweenMax.staggerTo(element.el, 0.75, { autoAlpha: 1 }, 0.2);
				TweenMax.staggerFromTo(element.el, 0.75, { x: '100%' }, { ease: Power3.easeOut, x: '0%' }, 0.2);
			}
		})

		requestAnimationFrame(this.update);
	}
}