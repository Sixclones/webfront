import TweenMax from 'gsap/TweenMax';

export default class Hamburger {
	constructor(_el, _menu) {
		this.toggleMenu = this.toggleMenu.bind(this);

		this.el = document.querySelector(_el);
		this.menu = document.querySelector(_menu);
		this.duration = 0.5;
		this.ease = Power2.easeOut;

		this.el.addEventListener('click', this.toggleMenu);
	}
	toggleMenu() {
		if (this.el.classList.contains('open')) {
			this.closeMenu();
		} else {
			this.openMenu();
		}
		this.el.classList.toggle('open');
	}
	openMenu() {
		TweenMax.to(this.menu, this.duration, { x: '-100%', ease: this.ease });
	}
	closeMenu() {
		TweenMax.to(this.menu, this.duration, { x: '0%', ease: this.ease });
	}
}