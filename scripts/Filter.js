export default class Filter {
	constructor(_wrapper) {
		this.render = this.render.bind(this);

		this.template = require('../hbs/listing-catalog.hbs');
		this.datas = require('./../datas/catalog.json');
		this.wrapper = document.getElementById(_wrapper);
		this.select = document.querySelector(`.${_wrapper}-sort-select`);
		this.select.addEventListener('change', this.render);
		this.value;

		this.render();
	}
	filter() {
		this.value = this.select.value;
		this.datas.sort((a, b) => {
			if (a[this.value] < b[this.value]) return -1;
			if (a[this.value] > b[this.value]) return 1;
			return 0;
		});
	}
	render() {
		this.filter();
		this.wrapper.innerHTML = this.template(this.datas);
	}
}