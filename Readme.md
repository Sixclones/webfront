# Webfront

Projet web pour les Gobelins dans le cadre du cours de Thibault Leporé.

## Installation

```sh
$ npm i
```
**Note : Nécessite [Node.js](https://nodejs.org/en/) et [npm](https://www.npmjs.com/)**

## Scripts Node

```sh
$ npm run dev
# Lance l'environnement de dév

$ npm run prod
# Compile le projet dans le dossier "dist"
```
Le premier script permet de développer en gardant chaque fichiers indépendant. Le dossier `dist` a pour but d'être mis sur le serveur, il représente l'intégralité du site minifié.  
**Note : il est quand même nécessaire de lancer son `localhost` pour ouvrir l'index html de `dist` à cause de l'AJAX.**

## Architecture

### Templating et contenu

Tout le corps de la maquette se trouve dans le dossier `hbs`.  
`home.hbs` créé le squelette du site et chaque partie est divisée dans un sous fichier. Ainsi les pages de contenu ne sont jamais trop longues et sont divisées par section.  
**Exception :**  
`listing-catalog.hbs` ne contient pas une section complète mais le template de listes de produits.


### Style

Dans le dossier `sass` trois dossiers :

#### `components`
Contient tout les éléments réutilisables du site, une flèche, les éléments de formulaires, un listing etc.

#### `global`
Correspond à ce qui va être appliqué partout. Media queries, reset et variables.

#### `parts`
Le style de chaque partie du site.

### Scripts JS

Tout est divisé en `class` et est instancié dans `index.js`.  
`html.js` permet d'injecter tout les templates de pages `hbs`.  


Bonne correction
