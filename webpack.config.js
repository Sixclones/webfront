var path = require("path");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const SpriteLoaderPlugin =  require('svg-sprite-loader/plugin');
const isDevelopment = process.env.NODE_ENV !== "production";

module.exports = {
  entry: {
    index: [
      "babel-polyfill",
      "./scripts/index.js",
    ],
  },
  output: {
    filename: "[name].js",
    path: path.resolve(__dirname, 'dist')
  },
  devtool: "source-map",
  module: {
    rules: [
      {
        test: /\.scss$/,
        use:[{
          loader: "style-loader"
        },
        {
          loader: "css-loader", options: {
            sourceMap: true
          }
        },
        {
          loader: "resolve-url-loader", options: {
            sourceMap: true
          }
        },
        {
          loader: "sass-loader", options: {
            sourceMap: true
          }
        }]
    },
    {
      test: /\.hbs$/,
      use:[
        {
          loader: 'handlebars-loader',
          options:{
            inlineRequires:"./img/",
            helperDirs:[
              __dirname + "/helpers",
            ]
          }
        }
      ]
    },

    {
      test: /\.js$/,
      exclude: /(node_modules)/,
      use: [
        {
          loader: 'babel-loader',
          options: {
            presets: ['es2015']
          }
        }
      ]
		},
    /*
    {
      test: /\.svg$/,
      loader: 'svg-sprite-loader',
      options: {
        extract: true,
        include: '/img/',
        spriteFilename: 'sprite-[hash:6].svg'
      }
    },*/

    {
			test: /\.(jpe?g|png|gif|svg|eot|woff|woff2)$/,
			exclude: /node_modules/,
			loader: "file-loader",
			options: {
				name: '[name].[ext]'
			}
    },
		{
			test: /\.json$/,
			loader: "json-loader",
			options: {
				name: '[name].[ext]'
			}
		}
    ]
	},
  devServer: {
      contentBase: path.resolve(__dirname, './dist/'),
      overlay: {
        warnings: true,
        errors: true
      },
      compress: false,
      port: 3000,
      publicPath:"/",
      clientLogLevel:"error",
      historyApiFallback: true,
      inline: true
  },
  plugins: [

    new HtmlWebpackPlugin({
      title:"Webfront",
      template:"hbs/index.hbs",
      filename:"index.html",
      minify:{
        collapseWhitespace:isDevelopment
      },
      showErrors:true,
			hash: true,
			
    }),
		new SpriteLoaderPlugin()
	],
};
